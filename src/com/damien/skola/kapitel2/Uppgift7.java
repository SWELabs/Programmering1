package com.damien.skola.kapitel2;

import java.util.Scanner;

/* Den här koden är skriven av Oscar Damien Olsen, Burgården NA2
 * Datum: 2017-09-05
 */

public class Uppgift7 {
	
	public static void main(String[] args) {
		
		// Skapar en scanner för andvändar input
		Scanner scanner = new Scanner(System.in);
		
		// Frågar om ett tecken att mata in
		System.out.print("Tecken: ");
		String input = scanner.nextLine();
		
		// Om input har mer än en bokstav, så säger programmet ifrån.
		if (input.length() > 1) {
			
			System.out.println("Du får bara skriva in en bokstav!");
			
		} else {
			
			// Omvandlar String till char, och char till int.
			char tecken = input.charAt(0);
			int kod = (int)tecken;
			
			// Printar koden i konsolen
			System.out.println(String.format("Tecknet %s har teckenkoden %s", tecken, kod));
			scanner.close(); // Stänger scannern
			
		}
	}
}
