package com.damien.skola.kapitel2;

/* Den här koden är skriven av Oscar Damien Olsen, Burgården NA2
 * Datum: 2017-08-29
 */

import java.util.InputMismatchException;
import java.util.Scanner;

public class Uppgift2 { // Uppgift 2.2
	
	public static void main(String[] args) {
		
		// Skapar en scanner som tar user input.
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Mata in ett tal > ");
		
		// Försöker och sedan fångar exceptions.
		try {
			
			// Definerar variabeln input som det andvändaren har matat in, och tar det g�nger sig sj�lv.
			int input = scanner.nextInt();
			int kvadrat = input * input;
			scanner.close(); // Stoppar input/scanner (för att förhindra memory leak)
			
			// Printar kvadraten av talet.
			System.out.println(String.format("Kvadraten av %s �r: %s", input, kvadrat));
			
		} catch (InputMismatchException e) {
			
			// Printar vad som gick fel (exception)
			System.out.println(String.format("Nagot gick fel!\n%s\nDu kan bara mata in siffror", e));
		}
		
		
	}
}
