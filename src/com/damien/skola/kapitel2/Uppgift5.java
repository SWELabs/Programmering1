package com.damien.skola.kapitel2;

import java.util.Scanner;

/* Den h�r koden �r skriven av Oscar Damien Olsen, Burg�rden NA2
 * Datum: 2017-08-31
 */

public class Uppgift5 {
	
	public static void main(String[] args) {
		
		try {
			//Skapar en scanner f�r inmatning
			Scanner scanner = new Scanner(System.in);
			
			// Fr�gar andv�ndaren efter dess namn
			System.out.print("Ange namn: ");
			String namn = scanner.nextLine();
			
			// Tar reda p� vilken siffra mellanslaget ligger
			int mellanslag = namn.indexOf(' ');
			
			// Plockar ut initialerna (F�rsta bokstaven i f�rnamn och efternamn)
			String initialer = namn.substring(0, 1) + "." + namn.substring(mellanslag + 1, mellanslag + 2);
			
			// Printar initialerna.
			System.out.println(String.format("%s har initialerna %s", namn, initialer));
			
			// St�nger scannern f�r att f�rhindra memory leak.
			scanner.close();
			
		} catch (StringIndexOutOfBoundsException e) {
			
			System.out.println(String.format("Rip! Ett fel uppstod:\n%s\nMata in 2 ord (t.ex: Damien Olsen)", e));
			
		}
	}
}
