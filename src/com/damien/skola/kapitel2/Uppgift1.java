package com.damien.skola.kapitel2;

/* Den här koden är skriven av Oscar Damien Olsen, Burgården NA2
 * Datum: 2017-08-29
 */

public class Uppgift1 { // Uppgift 2.1
	
	public static void main(String[] args) {
		
		//Definerar decimaltalen a & b
		double a = 2.1;
		double b = 3.1;
		
		//Adderar a & b
		double summa = a + b;
		
		/*
		 * text variabeln formaterar ihop a, b, summa.
		 * sedan så printar vi variabeln text
		 */
		String text = String.format("Tal A ar %s\nTal B ar %s\nSumman ar %s", a, b, summa);
		System.out.println(text);
	}
}
