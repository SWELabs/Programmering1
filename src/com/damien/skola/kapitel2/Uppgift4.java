package com.damien.skola.kapitel2;

import java.util.InputMismatchException;
import java.util.Scanner;

/* Den här koden är skriven av Oscar Damien Olsen, Burgården NA2
 * Datum: 2017-08-29
 */

public class Uppgift4 { // Uppgift 2.4
	
	public static void main(String[] args) {
		
		// Försöker och sedan fångar exceptions.
		try {
			
			// Skapar en scanner som tar user input.
			Scanner scanner = new Scanner(System.in);
			
			// Matar in 3 tal från andvändaren till konsolen
			System.out.print("Mata in ett tal > ");
			int a = scanner.nextInt();
			
			System.out.print("Och ett till > ");
			int b = scanner.nextInt();
			
			System.out.print("Och det sista > ");
			int c = scanner.nextInt();
			
			int antal = 3; // Antalet siffror
			
			//Räknar ut medelvärdet & summa
			int summa = a + b + c;
			double medel = (a + b + c) / (double) antal;
			
			// Printar resultatet till konsolen
			System.out.println(String.format("Summan ar: %s\nMedelvardet ar: %s", summa, medel));
			scanner.close(); // Stoppar input/scanner (för att förhindra memory leak)
			
		} catch (InputMismatchException e) {
			
			// Printar vad som gick fel (exception)
			System.out.println(String.format("Nagot gick fel!\n%s\nDu kan bara mata in siffror", e));
		}
	}
}
