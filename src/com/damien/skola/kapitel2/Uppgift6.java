package com.damien.skola.kapitel2;

import java.util.Scanner;

/* Den här koden är skriven av Oscar Damien Olsen, Burgården NA2
 * Datum: 2017-08-31
 */

public class Uppgift6 {
	
	public static void main(String[] args) {
		
		try {
			//Skapar en scanner för inmatning
			Scanner scanner = new Scanner(System.in);
			
			// Frågar användaren efter dess namn
			System.out.print("Ange namn: ");
			String namn = scanner.nextLine();
			
			// Tar reda på längden av namnet
			int length = namn.length();
			
			// Tar reda på vilken siffra mellanslaget ligger
			int mellanslag = namn.indexOf(' ');
			
			// Separerar förnamn och efternamn
			String efternamn = namn.substring(mellanslag + 1, length);
			String fornamn = namn.substring(0, mellanslag);
			
			// Printar initialerna.
			System.out.println(String.format("Fornamn: %s\nEfternamn: %s", fornamn, efternamn));
			
			// Stänger scannern för att förhindra memory leak.
			scanner.close();
			
		} catch (StringIndexOutOfBoundsException e) {
			
			System.out.println(String.format("Rip! Ett fel uppstod:\n%s\nMata in 2 ord (t.ex:  Damien Olsen)", e));
			
		}
	}
}

