package com.damien.skola.kapitel2;

/* Den här koden är skriven av Oscar Damien Olsen, Burgården NA2
 * Datum: 2017-08-29
 */

import java.util.InputMismatchException;
import java.util.Scanner;

public class Uppgift3 { // Uppgift 2.3
	
	public static void main(String[] args) {
		
		// Skapar en scanner som tar user input.
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Ange din alder: ");
		
		// Försöker och sedan fångar exceptions.
		try {
			
			// tar ålder som input, och sedan räknar ut matten.
			int input = scanner.nextInt();
			int nollade_sist = (input / 10) * 10;
			int nollade = nollade_sist + 10;
			int resultat = nollade - input;
			
			// Kollar om åldern är mindre än 0 (dvs negativt), om inte, så kör programmet normalt
			if (input < 0) {
				System.out.println("Du kan inte ha en negativ alder");
			} else {
				// Printar när man nollar, och om hur många år.
				System.out.println(String.format("Du fyller %s om %s ar! ", nollade, resultat));
				scanner.close(); // Stoppar input/scanner (för att förhindra memory leak)
			}
			
		} catch (InputMismatchException e) {
			
			// Printar vad som gick fel (exception)
			System.out.println(String.format("Nagot gick fel!\n%s\nDu kan bara mata in siffror", e));
		}
	}
}
