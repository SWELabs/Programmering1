package com.damien.skola.kapitel2;

import java.util.Scanner;

/* Den här koden är skriven av Oscar Damien Olsen, Burgården NA2
 * Datum: 2017-09-05
 */

public class Uppgift8 {
	
	public static void main(String[] args) {
		
		// Skapar scanner
		Scanner scanner = new Scanner(System.in);
		
		// Frågar om en stor bokstav
		System.out.print("Ange en stor bokstav: ");
		String input = scanner.nextLine();
		
		// Omvandlar input till char, och char till int som sedan blir lowercase.
		char bokstav = input.charAt(0);
		int lowerCase = (int)bokstav + 32;
		
		// Printar till konsolen
		System.out.println(String.format("Litet %s blir %s", bokstav, (char)lowerCase));
		scanner.close();
		
	}
}
