package com.damien.skola.kapitel2;

/* Den här koden är skriven av Oscar Damien Olsen, Burgården NA2
 * Datum: 2017-08-31
 */

public class ExempelArrayLoop {
	
	public static void main(String[] args) {
		
		// Skapar en array som heter matkassa, inne i den har vi Strings (mat)
		String[] matkassa = {
				"Banan",
				"Creme Fraische",
				"Broccoli",
				"Salad",
				"Isglass",
				"Yoghurt"
		};
		
		// Skapar en loop som går igenom hela arrayen/listan (matkassa)
		for (int i = 0; i < matkassa.length; i++) {
			
			// Printar varje String i matkassa
			System.out.println(matkassa[i]);
			
			// Om programmet hittar Salad i matkasse, så bryts loopen
			String mat = "Salad";
			if (mat == matkassa[i]) {
				break; // <--- Bryter loopen
			}
		}
	}
}
