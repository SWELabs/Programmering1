package com.damien.skola.kapitel1;

/* Den här koden är skriven av Oscar Damien Olsen, Burgården NA2
 * Datum: 2017-08-29
 */

public class Uppgift1 {
	
	public static void main(String[] args) {
		//Printar namn och adresss på skärmen - Uppgift 1.1
		System.out.println("Damien\nLankharvsgatan\n32");
	}
}
