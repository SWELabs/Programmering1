package com.damien.skola.kapitel1;

/* Den här koden är skriven av Oscar Damien Olsen, Burgården NA2
 * Datum: 2017-08-29
 */

public class Uppgift3 {
	
	public static void main(String[] args) {
		//Printar "JAVA" konst i konsolen - Uppgift 1.3
		System.out.println(
				    "JJJJJ   JJJ   J   J   JJJ"
				+ "\n    J  J   J  J   J  J   J"
				+ "\n    J  J   J   J J   J   J"
				+ "\nJ   J  JJJJJ   J J   JJJJJ"
				+ "\n JJJ   J   J    J    J   J");
	}
}
