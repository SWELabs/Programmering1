/*
* Written by Damien Olsen
* 2017-08-31
*/

// Importerar readline för inmatning
const readline = require('readline');

// Skapar input
const input = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Säger till andvändaren att mata in sin ålder
input.question('Ange din ålder: ', (age) => {

  /* Räknar ut när du nollade sist, och vilket tiotal det är du ska nolla nästa gång.
  * Räknar också ut hur många år kvar tills man nollar nästa gång.
  */
  var nolla_sist = Math.floor(age / 10) * 10;
  var nolla = nolla_sist + 10;
  var resultat = nolla - age;

  // Printar resultatet till konsolen.
  console.log(`Du fyller ${nolla} om ${resultat} år!`);
  input.close();

});
