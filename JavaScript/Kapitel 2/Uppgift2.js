/*
* Written by Damien Olsen
* 2017-08-31
*/

// Importerar readline för inmatning
const readline = require('readline');

// Skapar input
const input = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Säger till andvändaren att mata in ett tal
input.question('Mata in ett tal: ', (tal) => {

  // Räknar ut kvadraten av talet och printar till konsolen.
  var resultat = tal ** 2;
  console.log(`Kvadraten på ${tal} är: ${resultat}`);

  // Stänger inmatningen
  input.close();
});
