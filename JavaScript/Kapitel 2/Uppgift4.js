/*
* Written by Damien Olsen
* 2017-08-31
*/

// Importerar readline för inmatning
const readline = require('readline');

// Skapar input
input = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Säger till andvändaren att man ska mata in talen
input.question('Mata in det första talet: ', (a) => {
  input.question('Mata in det andra talet: ', (b) => {
    input.question('Mata in det tredje talet: ', (c) => {

      // Räknar ut medelvärdet
      const antal = 3;
      var summa = parseInt(a) + parseInt(b) + parseInt(c);
      var medel = summa / antal;

      // Printar medelvärdet och summan till konsolen.
      console.log(`Summan blir: ${summa}\nMedelvärdet blir: ${medel}`);
      input.close();

    });
  });
});
