/*
* Written by Damien Olsen
* 2017-08-31
*/

// Skapar variablerna a & b, och ger dom ett värde
var a = 2.1;
var b = 3.1;

// Skapar en variabel som heter summa, och adderar a & b
var summa = a + b;

// Printar det till konsolen
console.log(`Tal A är ${a}\nTal B är ${b}\nSumman är ${summa}`);
